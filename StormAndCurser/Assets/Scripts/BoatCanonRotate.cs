﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Networking;

public class BoatCanonRotate : MonoBehaviour
{
    #region Variables
    float vRotation;
    float hRotation;
    [SerializeField]
    float XRot=0;
    float cnnXRot=0;
    float XRotInitial;
    float cnnXRotInitial;
    [SerializeField]
    float YRot=0;
    float cnnYRot=0;
    float YRotInitial;
    float cnnYRotInitial;
    Quaternion rotYQut=Quaternion.identity;
    Quaternion cnnRotYQut=Quaternion.identity;
    Quaternion destRot=Quaternion.identity;
    Quaternion cnnDestRot=Quaternion.identity;
    [SerializeField]
    float  YMaxAngle=140;
    
    [SerializeField]
    float XMaxAngle=140;
    
    [SerializeField]
    float YRotationSpeed=3;
    [SerializeField]
    float XRotationSpeed=3;
    [SerializeField]
    float lerpRotationXAmount=2;
    [SerializeField]
    float lerpRotationYAmount=3;
   public Transform objectToRotate;
   public Transform CanonPivot;
    #endregion
    #region Propeties
    #endregion
    #region  Functions
    // Start is called before the first frame update
    void Start()
    {
       /*  XRotInitial=objectToRotate.eulerAngles.x;
        YRotInitial=objectToRotate.eulerAngles.y;
        YRot=YRotInitial;
        XRot=XRotInitial;
        cnnXRotInitial=CanonPivot.eulerAngles.x;
        cnnYRotInitial=CanonPivot.eulerAngles.y;
        cnnXRot=cnnXRotInitial;
        cnnYRot=cnnYRotInitial;*/
        XRotInitial=objectToRotate.localEulerAngles.x;
        YRotInitial=objectToRotate.localEulerAngles.y;
        YRot=YRotInitial;
        XRot=XRotInitial;
        cnnXRotInitial=CanonPivot.localEulerAngles.x;
        cnnYRotInitial=CanonPivot.localEulerAngles.y;
        cnnXRot=cnnXRotInitial;
        cnnYRot=cnnYRotInitial;
        
    }

    // Update is called once per frame
    void Update()
    {
        rotate();
    }
    void rotate()
    {
     /*  hRotation=Input.GetAxis("Mouse X")*YRotationSpeed*Time.deltaTime;
      YRot+=hRotation;
      YRot=Mathf.Clamp(YRot,-YMaxAngle+YRotInitial,YMaxAngle+YRotInitial);
      rotYQut=Quaternion.Euler(0,YRot,0);
      vRotation=Input.GetAxis("Mouse Y")*XRotationSpeed*Time.deltaTime;
      XRot-=vRotation;
      XRot=Mathf.Clamp(XRot,-XMaxAngle+XRotInitial,XMaxAngle+XRotInitial);
      destRot=rotYQut*Quaternion.Euler(XRot,0,0);
      objectToRotate.rotation=destRot;*/
      //local
      hRotation=Input.GetAxis("Mouse X")*YRotationSpeed*Time.deltaTime;
      YRot+=hRotation;
      YRot=Mathf.Clamp(YRot,-YMaxAngle+YRotInitial,YMaxAngle+YRotInitial);
      rotYQut=Quaternion.Euler(0,YRot,0);
      vRotation=Input.GetAxis("Mouse Y")*XRotationSpeed*Time.deltaTime;
      XRot-=vRotation;
      XRot=Mathf.Clamp(XRot,-XMaxAngle+XRotInitial,XMaxAngle+XRotInitial);
      destRot=rotYQut*Quaternion.Euler(XRot,0,0);
      objectToRotate.localRotation=destRot;
     rotateCanonPvt();
    }
    void rotateCanonPvt()
    {
       /*  cnnYRot+=hRotation;
      cnnYRot=Mathf.Clamp(cnnYRot,-YMaxAngle+cnnYRotInitial,YMaxAngle+cnnYRotInitial);
      cnnRotYQut=Quaternion.Euler(0,cnnYRot,0);
      cnnXRot-=vRotation;
      cnnXRot=Mathf.Clamp(cnnXRot,-XMaxAngle+cnnXRotInitial,XMaxAngle+cnnXRotInitial);
      cnnDestRot=cnnRotYQut*Quaternion.Euler(cnnXRot,0,0);
      CanonPivot.rotation=cnnDestRot;*/
      //local
       cnnYRot+=hRotation;
      cnnYRot=Mathf.Clamp(cnnYRot,-YMaxAngle+cnnYRotInitial,YMaxAngle+cnnYRotInitial);
      cnnRotYQut=Quaternion.Euler(0,cnnYRot,0);
      cnnXRot-=vRotation;
      cnnXRot=Mathf.Clamp(cnnXRot,-XMaxAngle+cnnXRotInitial,XMaxAngle+cnnXRotInitial);
      cnnDestRot=cnnRotYQut*Quaternion.Euler(cnnXRot,0,0);
      CanonPivot.localRotation=cnnDestRot;

        
    }
    #endregion
}
