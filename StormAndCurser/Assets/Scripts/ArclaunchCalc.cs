﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArclaunchCalc : MonoBehaviour
{
    #region Variables
    [SerializeField]
    float angleWithX;
    LineRenderer lr;
    [SerializeField]
    float speed=20;
    [SerializeField]
    float gravity=9.8f;
    [SerializeField]
    int pointNom=10;
    private float range;
    [SerializeField]
    private Vector3 luanchEuler;
    private float collisonTime;
    [SerializeField]
    private Transform MuzzlePoint;
    private Vector3 initPos;
   [SerializeField]
   Vector3[] points;
    #endregion
    #region Functions
    private void Awake()
    {
        initPos=MuzzlePoint.position;
        lr=GetComponent<LineRenderer>();
        points=new Vector3[pointNom];
        luanchEuler=MuzzlePoint.eulerAngles;
    }
    private void Update()
    {
       calcPoints();
    }
    public void calcPoints()
    {
        luanchEuler=MuzzlePoint.eulerAngles;
        float angle =Mathf.Acos(Vector3.Dot(Vector3.up,MuzzlePoint.forward));
         angleWithX=Mathf.Acos(Vector3.Dot(MuzzlePoint.forward,Vector3.right));
        float cosB=Mathf.Cos(angleWithX);
        float sinB=Mathf.Sin(angleWithX);
        float cosA=Mathf.Cos(angle);
        float sinA=Mathf.Sin(angle);
        collisonTime=2*(speed*sinA/gravity);
        float TimeInterval=collisonTime/pointNom;
        lr.positionCount=pointNom;
        for(int i=0;i<pointNom;i++)
        {
            points[i].x=speed*sinA*cosB*TimeInterval*i+initPos.x;
            points[i].z=speed*sinA*sinB*TimeInterval*i+initPos.z;
            points[i].y=speed*cosA*TimeInterval*i-.5f*gravity*(TimeInterval*i)*(TimeInterval*i)+initPos.y;;
            if(points[i].y<0)
            {
                points[i].y=0;
                points[i].x=points[i-1].x;
                points[i].z=points[i-1].z;
            }
            lr.SetPosition(i,points[i]);
            
        }
        

    }
    #endregion
}
